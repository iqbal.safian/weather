1. Clone this repo by using Git command:
   <q>git clone https://gitlab.com/iqbal.safian/weather</q>

2. Execute following npm/yarn command
   <q>npm install</q> or <q>yarn install</q>

3. Fill in the appId of openweathermap in the openweather.js file.

4. Do <q>yarn start</q>
