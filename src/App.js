import React from 'react';
import './App.css';
import './w3.css';
import WeatherCities from './components/WeatherCities';
import AddCity from './components/AddCity';

function App() {
  return (
    <div className="App">
      <WeatherCities />
      <AddCity />
    </div>
  );
}

export default App;
