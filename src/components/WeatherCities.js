import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import City from './City';

function WeatherCities(props) {
  const { cities } = useSelector(state=>state);
  const dispatch = useDispatch();
  return (
    <div className="grid-row">
      {
        cities.map((city, index)=>{
          console.log(city);
          return(
              <City
                key={index}
                id={city.id}
                name={city.name}
                interval={city.interval}
                main={city.main}
                weather = {city.weather}
                sys = {city.sys}
                remove={()=>dispatch({type: 'REMOVE_CITY', payload: `${index}`})}
               />
          )
        })
      }
    </div>
  )
}

export default WeatherCities;
