import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import appVar from '../openweather';

function City (props) {
  console.log(props);
  let [main, setMain] = useState(props.main);
  let [weather, setWeather] = useState(props.weather);
  let [loading, setLoading] = useState(false);
  // let [timeLeft, setTimeLeft] = useState(props.interval);

  useEffect(()=>{
    const myInterval = setInterval(async ()=> {
      setLoading(true);
      let getWeatherData = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=${props.id}&APPID=${appVar.appId}&units=metric`);
      const response = await getWeatherData.json()
        .then(data=>{
          if (data.cod === 200) {
            setMain(data.main);
            setWeather(data.weather[0]);
          }
        })
      setLoading(false);
    }, parseInt(props.interval*1000));
    return () => clearInterval(myInterval);
  })

  let image = "http://openweathermap.org/img/wn/" + weather.icon + "@2x.png"

  // useEffect(()=>{
  //   const myCountDown = setInterval(()=>{
  //     setTimeLeft(timeLeft=>timeLeft-1)
  //   }, 1000);
  //
  //   return () => clearInterval(myCountDown)
  //   setTimeLeft(props.interval)
  // })

  return(
    <div className="grid-col" style={{minWidth:'300px', position: 'relative'}}>
      <div className={classNames(loading?'overlay':'')}></div>
      <div style={{textAlign:'left'}}>{props.name}</div>
      <div
        className="w3-hover-shadow parent"
        style={
          {
            borderStyle:'solid',
            borderWidth: '1px',
            borderColor: 'rgba(160, 160, 255)',
            paddingTop: '10px',
            paddingBottom: '10px'
          }
        }
      >
        <div>
          <img src={image} alt={weather.icon} />
        </div>
        <div> Humidity: {main.humidity} </div>
        <div> Min Temperature: {main.temp_min} <span>&#8451;</span></div>
        <div> Max Temperature: {main.temp_max} <span>&#8451;</span></div>
        <div className="child"><button className="button-remove" onClick={props.remove}>remove</button></div>
      </div>
      <div className="row">
        <div className="column w3-left">
          {main.temp} <span>&#8451;</span>
        </div>
        <div  className="column w3-right">
          {weather.main}
        </div>
      </div>
    </div>
  )
}

export default City;
