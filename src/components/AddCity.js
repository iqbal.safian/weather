import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import appVar from '../openweather';

function AddCity(props) {
  let [cityName, setCityName] = useState('');
  let [interval, setInterval] = useState(20);
  const dispatch = useDispatch();

  async function addCity() {
    let id, main, sys, name, weather;

    if (cityName) {
      let getWeatherData = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=${appVar.appId}&units=metric`);
      const response = await getWeatherData.json()
        .then(data=>{
          if (data.cod === 200) {
            id = data.id;
            name = data.name;
            main = data.main;
            sys = data.sys;
            weather = data.weather[0];
          } else if (data.cod === 404) {
            console.log('errr');
          } else {
            alert('No city found')
          }
        })
        .catch(err => {
          console.log('error');
        });

      if (id) {
        dispatch({type: 'ADD_CITY', payload: {id, name, interval, main, weather, sys}})
        setCityName('');
        setInterval(20);
      } else {

      }
    }
  }
  return(
    <div>
      <input placeholder="Type city" value={cityName} onChange={e=>setCityName(e.target.value)} style={{margin: '10px'}} />
      <select placeholder="Select interval" value={interval} onChange={e=>setInterval(Number(e.target.value))} style={{margin: '10px'}} >
        <option value="20">20 seconds</option>
        <option value="40">40 seconds</option>
        <option value="60">60 seconds</option>
      </select>
      <button onClick={addCity} style={{margin: '10px'}} >Add</button>
    </div>
  )
}

export default AddCity;
