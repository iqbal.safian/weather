import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import WeatherCities from './components/WeatherCities';
import AddCity from './components/AddCity';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('App', () => {
  let wrapper;
  beforeEach(()=>wrapper=shallow(<App />));

  it('should render a <div />', () => {
    expect(wrapper.find('div').length).toEqual(1);
  });

  it('should render the WeatherCities component', () => {
    expect(wrapper.containsMatchingElement(<WeatherCities />)).toEqual(true);
  });

  it('should render the AddCity component', () => {
    expect(wrapper.containsMatchingElement(<AddCity />)).toEqual(true);
  })
});
