const ADD_CITY = "ADD_CITY";
const REMOVE_CITY = "REMOVE_CITY";

const initialState = {
  cities: []
}

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_CITY:
      let newCity = action.payload;
      console.log(newCity);
      state.cities.push(newCity);
      return {
        ...state,
        newCity
      }
    case REMOVE_CITY:
      state.cities = state.cities.filter((_, index) => index !== parseInt(action.payload))
      return {
        ...state
      }
    default:
      return {
        ...state
      }
  }
}
