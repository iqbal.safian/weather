import React from 'react';
import { shallow } from 'enzyme';
import WeatherCities from '../components/WeatherCities';
import City from '../components/City';

describe('WeatherCities', () => {
  let wrapper;
  beforeEach(()=>wrapper=shallow(<WeatherCities />));

  it('should render correctly', ()=> expect(wrapper).toMatchSnapshot());

  it('should render City component', () => {
    expect(wrapper.containsMatchingElement(<City />));
  })
})
