import React from 'react';
import { shallow } from 'enzyme';
import City from '../components/City';

describe('City', () => {
  let wrapper;
  beforeEach(()=>wrapper=shallow(<City />));

  it('should render correctly', () => expect(wrapper).toMatchSnapshot());
})
